﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour 
{
    //Create a reference to the CoinPoofPrefab

	public GameObject coinpoof;

    public void OnCoinClicked() {
        // Instantiate the CoinPoof Prefab where this coin is located
		// Make sure the poof animates vertically
		Instantiate (coinpoof, transform.position, Quaternion.Euler(270, 0, 90));
        // Destroy this coin. Check the Unity documentation on how to use Destroy
		Destroy (gameObject);
    }

}
