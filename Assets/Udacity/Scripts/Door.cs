﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour 
{
    // Create a boolean value called "locked" that can be checked in OnDoorClicked() 
	public bool locked;
    // Create a boolean value called "opening" that can be checked in Update() 
	public bool opening;
	// crate an animator for the door
	public Animator doorOpen;

	//Define two audio sources for door to open an close sounds
	public AudioSource lockAudio;
	public AudioSource openAudio;


    void Update() {
        // If the door is opening and it is not fully raised play the lock sound
		if (opening == true) {
			lockAudio.Play ();
		}            
    }

    public void OnDoorClicked() {
        // If the door is clicked and unlocked
		if (locked == false) {
			// open the door by calling OpenDoor
			OpenDoor ();

		}
		// (optionally) Else
		else {
			// Play a sound to indicate the door is locked
			lockAudio.Play ();
		}
    }

	public void OpenDoor() {
		// sets the doors state to opening
		opening = true;
		// Play door open sound and animate the door raising up
		openAudio.Play ();
		doorOpen.SetTrigger ("Open Door");
		// rest open to false now that the door is open
		opening = false;
	}

    public void Unlock()
    {
        // You'll need to set "locked" to false here, also doubles as a key held status
		locked = false;
    }
}
