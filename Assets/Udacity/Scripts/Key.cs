﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour 
{
    //Create a reference to the KeyPoofPrefab and Door
	public GameObject keyPoof; // this is creating a game object varable which I populate with a KeyPoof object
	public Door door; // this is referencing the door game object within the scene

	void Update()
	{
		//Not required, but for fun why not try adding a Key Floating Animation here :)
	}

	public void OnKeyClicked()
	{
        // Instatiate the KeyPoof Prefab where this key is located
		// Make sure the poof animates vertically
		Instantiate (keyPoof, transform.position, Quaternion.Euler(270, 0, 90));
        // Call the Unlock() method on the Door
		door.Unlock ();
        // Destroy the key. Check the Unity documentation on how to use Destroy
		Destroy(gameObject);
    }

}
